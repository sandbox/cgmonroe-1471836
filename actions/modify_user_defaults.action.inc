<?php
/**
 * @file
 * Defines the modify user default notification settings action.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "user" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_modify_user_defaults_action_info() {
  return array(
    'notifications_utils_modify_user_defaults_action' => array(
      'description' => t("Modify a user's default send method and/or interval"),
      'type' => 'user',
      'configurable' => TRUE,
      'permissions' => array('administer notifications'),
    )
  );
}

/**
 * Implementation of "action_function"_form hook
 *
 * Calls the common form function that uses notifications to create it.
 *
 * @param unknown_type $context
 */
function notifications_utils_modify_user_defaults_action_form($context) {
  $f = notifications_utils_subscribe_form('author', $context);
  $form = array();
  $form['notifications'] = $f['notifications'];
  $form['notifications']['#title'] = t("Notificaton Defaults");
  $form['notifications']['#description'] =
    t("Select the notification defaults for the selected users below.");
  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_modify_user_defaults_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_modify_user_defaults_action_submit($form, $form_state) {
  $results = array(
    'send_method' => $form_state['values']['send_method'],
    'send_interval' => $form_state['values']['send_interval'],
  );
  return $results;
}
/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_modify_user_defaults_action(&$account, $context) {
  global $user;

  $options = array();
  $send_method = $context['send_method'];
  if ( $send_method != 'user_default' ) {
    $options['messaging_default'] = $send_method;
  }
  $send_interval = $context['send_interval'];
  if ( $send_interval >= 0 ) {
    $options['notifications_send_interval'] = $send_interval;
  }
  if ( ! empty( $options ) ) {
    $u = user_save($account, $options);
  }
  if ( $u && $user->uid == $account->uid ) {
    $user = $u;
  }
}
