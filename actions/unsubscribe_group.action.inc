<?php
/**
 * @file
 * Defines the unsubscribe action for 'grouptype' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "user" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_unsubscribe_group_action_info() {
  if ( notifications_utils_type_enabled("og_notifications", 'grouptype')) {
    return array(
      'notifications_utils_unsubscribe_group_action' => array(
        'description' => t("Unsubscribe user from a group's content."),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}

/**
 * Implementation of "action_function"_form hook
 *
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_group_action_form($context) {
  global $user;

  module_load_include('inc', 'og_notifications', 'og_notifications.pages');
  $groups =  og_all_groups_options();
  $form = og_notifications_add_form(NULL, $user, $groups);
  unset($form['account']);
  unset($form['subscription']['submit']);
  unset($form['subscription']['send_interval']);
  unset($form['subscription']['send_method']);
  $form['subscription']['#title'] = t("Unsubscribe from a Group's content");
  $form['subscription']['#description'] =
    t("Select the group and content type to unsubscribe the selected users from.");

  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_group_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_group_action_submit($form, $form_state) {
  $results = array();
  $values = $form_state['values'];

  $subscription = new stdClass;
  $subscription->type = 'grouptype';

  $subscription->fields = array(
    'group' => (string) $values['subscription']['group'][0],
  );

  if ($values['subscription']['node_type'][0] == 'all') {
    $types = array_filter(
      variable_get('og_notifications_content_types', array()));
    foreach ($types as $type) {
      $sub = clone $subscription;
      $sub->fields['type'] = $type;
      $results['subscriptions'][] = $sub;
    }
  }
  else {
    $subscription->fields['type'] = $values['subscription']['node_type'][0];
    $results['subscriptions'][] = $subscription;
  }

  return $results;
}
/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_group_action(&$account, $context) {
  foreach ( $context['subscriptions'] as $subscription ) {
    $params = array(
      'type' => $subscription->type,
      'uid' => $account->uid,
    );
    $conditions = array(
      'group' => $subscription->fields['group'],
      'type' => $subscription->fields['type'],
    );
    $subs = notifications_get_subscriptions($params, $conditions, TRUE, 'value');
    if ( empty($subs) ) {
      return;
    }
    $sub = reset($subs);
    if (! empty($sub) ) {
      notifications_delete_subscription($sub->sid);
    }
  }
}
