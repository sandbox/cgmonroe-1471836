<?php
/**
 * @file
 * Defines the subscribe action for 'thread' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "node" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_subscribe_thread_action_info() {
  if ( notifications_utils_type_enabled('notifications_content', 'thread')) {
    return array(
      'notifications_utils_subscribe_thread_action' => array(
        'description' => t("Subscribe user to a node's comments thread."),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}

/**
 * Implementation of "action_function"_form hook
 *
 * Calls the common form function that uses notifications to create it.
 *
 * @param unknown_type $context
 */
function notifications_utils_subscribe_thread_action_form($context) {
  $form = notifications_utils_subscribe_form('thread', $context);
  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_thread_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_thread_action_submit($form, $form_state) {
  module_load_include('inc', 'notifications', 'notifications.node');

  $results = notifications_utils_subscribe_form_submit($form, $form_state);
  $title = $results['subscription']['fields'][0]['value'];
  $nid = notifications_node_title2nid($title);
  $node = node_load($nid);
  $results['notification_node'] = $node;
  return $results;
}

/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_subscribe_thread_action( &$account, $context ) {
  $node = $context['notification_node'];
  if ( ! node_access('view', $node, $account) ) {
    return;  // user can't view node.. our work is done.
  };
  $conditions = array(
    'nid' => $node->nid,
  );
  notifications_utils_save_subscription($account, $context, $conditions);
}
