<?php
/**
 * @file
 * Defines the subscribe action for 'nodetype' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "user" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_subscribe_content_type_action_info() {
  if ( notifications_utils_type_enabled('notifications_content', 'nodetype')) {
    return array(
      'notifications_utils_subscribe_content_type_action' => array(
        'description' => t('Subscribe user to a content type.'),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}

/**
 * Implementation of "action_function"_form hook
 *
 * Calls the common form function that uses notifications to create it.
 *
 * @param unknown_type $context
 */
function notifications_utils_subscribe_content_type_action_form($context) {
  $form = notifications_utils_subscribe_form('nodetype', $context);
  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_content_type_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_content_type_action_submit($form, $form_state) {
  $results = notifications_utils_subscribe_form_submit($form, $form_state);
  return $results;
}
/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_subscribe_content_type_action(&$account, $context) {
  $conditions = array(
    'type' => $context['subscription']['fields'][0]['value'],
  );
  notifications_utils_save_subscription($account, $context, $conditions);
}
