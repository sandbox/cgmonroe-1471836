<?php
/**
 * @file
 * Defines the unsubscribe action for 'typeauthor' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "user" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_unsubscribe_type_author_action_info() {
  if ( notifications_utils_type_enabled('notifications_content', 'typeauthor')) {
    return array(
      'notifications_utils_unsubscribe_type_author_action' => array(
        'description' => t("Unsubscribe user from an author's postings of a specific content type."),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}

/**
 * Implementation of "action_function"_form hook
 *
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_type_author_action_form($context) {
  global $user;

  module_load_include('inc', 'notifications', 'notifications.pages');
  $form = notifications_add_subscription_form( NULL, $user, 'typeauthor');
  unset($form['account']);
  unset($form['#redirect']);
  unset($form['buttons']);
  unset($form['notifications']);
  $form['info']['#title'] = t("Unsubscribe from an Author / Content Type");
  $form['info']['#description'] =
    t("Select the author and content type to unsubscribe the selected users from.");
  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_type_author_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_type_author_action_submit($form, $form_state) {
  module_load_include('inc', 'notifications', 'notifications.node');

  $author = $form_state['values']['fields'][0]['value'];
  $account = user_load(array('name' => $author ));
  $results = array();
  $results['subscription_parameters'] = array(
    'type' => $form_state['values']['type'],
    'uid' => '',
  );
  $results['subscription_conditions'] = array(
    'author' => $account->uid,
    'type' => $form_state['values']['fields'][1]['value'],
  );
  return $results;
}
/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_type_author_action(&$account, $context) {
  $params = $context['subscription_parameters'];
  $params['uid'] = $account->uid;
  $conditions = $context['subscription_conditions'];
  $subs = notifications_get_subscriptions($params, $conditions, TRUE, 'value');
  if ( empty($subs) ) {
    return;
  }
  $subscription = reset($subs);
  if (! empty($subscription) ) {
    notifications_delete_subscription($subscription->sid);
  }
}
