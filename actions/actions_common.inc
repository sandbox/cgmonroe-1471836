<?php
/**
 * @file
 * Common functions used by all actions.
 */

define ("N_U_DEBUG", 0 );
/**
 * Include all action definition files.
 *
 * Note: does not cache files in variable table like VBO.
 */
function _notifications_utils_load_actions() {
  static $files = NULL;

  if (!empty($files)) {
    return $files;
  }
//  $files = unserialize(variable_get('notifications_utils_actions', NULL));
  if (empty($files)) {
    foreach (file_scan_directory(drupal_get_path('module', 'notifications_utils') . '/actions', '\.action\.inc$') as $file) {
      list($files[], ) = explode('.', $file->name);
    }
//    variable_set('notifications_utils_actions', serialize($files));
  }
  foreach ($files as $file) {
    module_load_include('inc', 'notifications_utils', "actions/$file.action");
  }
  return $files;
}

/**
 * Common function used by actions
 *
 * Calls the notifications_add_subscription_form for basic form.  Then
 * modifies it for VBO actions.
 *
 * @param unknown_type $context
 */
function notifications_utils_subscribe_form($type, $context) {
  global $user;

  module_load_include('inc', 'notifications', 'notifications.pages');
  $form = notifications_add_subscription_form( NULL, $user, $type);
  unset($form['account']);
  unset($form['#redirect']);
  unset($form['buttons']);
  $form['notifications']['send_interval']['#options'][-1] = 'User Default';
  $form['notifications']['send_interval']['#default_value'] = -1;
  $form['notifications']['send_method']['#options']['user_default'] =
    'User Default';
  $form['notifications']['send_method']['#default_value'] = 'user_default';
  $form['overwrite_subscription'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace existing subscriptions with these settings.'),
    '#description' => t('If selected, any users who already have a ' .
      'subscriptiion will have this replaced with the specified settings'),
  );
  $form['verbose'] = notifications_utils_verbose_option();
  return $form;
}
/**
 * Implementation of "action_function"_submit hook
 * Process the action parameters from the form using Notifications module
 * code plus some clean up.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_form_submit($form, $form_state) {
  module_load_include('inc', 'notifications', 'notifications.pages');
  notifications_add_subscription_form_validate( $form, $form_state );
  $subscription = array(
    'type' => $form_state['values']['type'],
    'fields' => $form_state['field_values'],
  );
  if ( $form_state['values']['send_method'] != 'user_default' ) {
    $subscription['send_method'] = $form_state['values']['send_method'];
  }
  if ( $form_state['values']['send_interval'] > -1 ) {
    $subscription['send_interval'] = $form_state['values']['send_interval'];
  }
  return array(
    'subscription' => $subscription,
    'overwrite_subscription' =>
      ! empty($form_state['values']['overwrite_subscription']),
    'verbose' =>
      ! empty($form_state['values']['verbose']),
      );
}
/**
 * Save a subscription with option to not overwrite existing subscriptions.
 *
 * @param UserObject $account The user being processed.
 * @param Array $Context The action context with Subscription info
 * @param Array $condition The field conditions to use to find existing subs
 */
function notifications_utils_save_subscription( $account,
                                                $context,
                                                $conditions = NULL ) {

  $subscription = $context['subscription'];
  $subscription['uid'] = $account->uid;
  $overwrite = $context['overwrite_subscription'];
  $verbose = $context['verbose'];
  if ( N_U_DEBUG ) {
    watchdog("notifi_utils",
      "save_subscription: uid=" . $account->uid .
      " overwrite= " . $overwrite .
      " subscription=<pre>" . print_r($subscription,TRUE) . "</pre>",
      NULL, WATCHDOG_DEBUG );
  }

  $params = array(
    'type' => $subscription['type'],
    'uid' => $account->uid,
  );
  $update = FALSE;
  $subs = notifications_get_subscriptions($params, $conditions, TRUE, 'value');
  if ( ! empty( $subs ) ) {
    if ( ! $overwrite ) {
      if ( N_U_DEBUG ) {
        watchdog("notifi_utils",
          "save_subscription: Subscription not added because there is an ' .
          'existing subscription. " .
          " subscription=<pre>" . print_r($subs,TRUE) . "</pre>",
          NULL,  WATCHDOG_DEBUG );
      }
      if ( $verbose ) {
        drupal_set_message(t('User @user: Subscription not added because ' .
          'one already exists.', array('@user' => $account->name)), 'status');
      }
      return;  // Already subscribed... do nothing.
    };
    $existing_sub = reset($subs);
    $subscription['sid'] = $existing_sub->sid;
    $update = TRUE;
  }

  $sub = notifications_utils_convert_sub( $subscription );
  if ( notifications_user_allowed('subscription', $account, $sub)) {
    $result = notifications_save_subscription($subscription);
    if ( $result === FALSE ) {
      $db_action = $update ? t("updated") : t("created");
      drupal_set_message(t('Subscription for @user was not @action.', array(
        '@user' => $account->name, '@action' => $db_action)), 'error');
    }
    elseif ( N_U_DEBUG ) {
      watchdog("notifi_utils",
        "save_subscription: Subscription was added.", NULL, WATCHDOG_DEBUG );
    }
  }
  elseif ( N_U_DEBUG ) {
    $rc = notifications_user_allowed('subscription', $account, $subscription);
    watchdog("notifi_utils",
      "save_subscription: Subscription not added notifications_user_allowed " .
      "denied access.  RC='<pre>" . print_r($rc,TRUE) . "</pre>'",
      NULL, WATCHDOG_DEBUG );
  }
  elseif ( $verbose ) {
    drupal_set_message(t('User @user: Subscription not added because ' .
      'they do not have correct permissions.',
      array('@user' => $account->name)), 'status');
  }
}
/**
 * Test if an event type is enabled.
 *
 * @param String $module The module that contains the event type
 * @param String $event_type The event type name.
 */
function notifications_utils_type_enabled( $module, $event_type ) {
  if ( module_exists($module)) {
    $type_info = notifications_subscription_types($event_type);
    if ( ! empty($type_info) && ! $type_info['disabled'] ) {
      return TRUE;
    }
  }
  return FALSE;
}
/**
 * Utility to convert the fields of a "form" version of a subscription to the
 * format the access check needs.
 *
 * @param Array $subscription
 * @return The reformatted subscription.
 */
function notifications_utils_convert_sub( $subscription ) {
  $subscription = (object) $subscription;
  $fields = array();
  foreach ( $subscription->fields as $key => $value ) {
    if ( is_array($value) ) {
      $fields[$value['type']] = $value['value'];
    }
  }
  $subscription->fields = $fields;
  return $subscription;
}
/**
 * Utility function to get the verbose option form element.
 */
function notifications_utils_verbose_option() {
  return array(
  '#type' => 'checkbox',
  '#title' => t('Show Drupal messages about skipped subscriptions.'),
  '#description' => t("If selected, a Drupal message will be displayed for " .
    "each user who do not have a subscription created. Users can be " . "
    'skipped' because they have an existing subscription or do not have " .
    "the right to create a subscription.  Note that this may create a lot ".
    "of messages."),
  );
}