<?php
/**
 * @file
 * Defines the unsubscribe action for 'thread' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "user" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_unsubscribe_thread_action_info() {
  if ( notifications_utils_type_enabled('notifications_content', 'thread')) {
    return array(
      'notifications_utils_unsubscribe_thread_action' => array(
        'description' => t("Unsubscribe user from a node's comment thread."),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}
/**
 * Implementation of "action_function"_form hook
 *
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_thread_action_form($context) {
  global $user;

  module_load_include('inc', 'notifications', 'notifications.pages');
  $form = notifications_add_subscription_form( NULL, $user, 'thread');
  unset($form['account']);
  unset($form['#redirect']);
  unset($form['buttons']);
  unset($form['notifications']);
  $form['info']['#title'] = t("Unsubscribe from a Comment Thread");
  $form['info']['#description'] =
    t("Select the thread's node to unsubscribe the selected users from.");

  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_thread_action_validate($form, $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_unsubscribe_thread_action_submit($form, $form_state) {
  module_load_include('inc', 'notifications', 'notifications.node');

  $title = $form_state['values']['field_values'][0]['value'];
  $nid = notifications_node_title2nid($title);

  $results = array();
  $results['subscription_parameters'] = array(
    'type' => $form_state['values']['type'],
    'uid' => '',
  );
  $results['subscription_conditions'] = array(
    'nid' => $nid,
  );
  return $results;
}
/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_unsubscribe_thread_action(&$account, $context) {
  $params = $context['subscription_parameters'];
  $params['uid'] = $account->uid;
  $conditions = $context['subscription_conditions'];
  $subs = notifications_get_subscriptions($params, $conditions, TRUE, 'value');
  if ( empty($subs) ) {
    return;
  }
  $subscription = reset($subs);
  if (! empty($subscription) ) {
    notifications_delete_subscription($subscription->sid);
  }
}
