<?php
/**
 * @file
 * Defines the subscribe action for 'grouptype' subscriptions.
 */

/**
 * Info about this actions... used to build hook_action_info in main program.
 *
 * This is used in "node" type views with VBO actions.
 * Requires the user to have "administer notifications" permission.
 */
function notifications_utils_subscribe_group_action_info() {
  if ( notifications_utils_type_enabled("og_notifications", 'grouptype')) {
    return array(
      'notifications_utils_subscribe_group_action' => array(
        'description' => t("Subscribe user to a groups's content."),
        'type' => 'user',
        'configurable' => TRUE,
        'permissions' => array('administer notifications'),
      )
    );
  }
}

/**
 * Implementation of "action_function"_form hook
 *
 * Calls the common form function that uses notifications to create it.
 *
 * @param unknown_type $context
 */
function notifications_utils_subscribe_group_action_form($context) {
  global $user;

  module_load_include('inc', 'og_notifications', 'og_notifications.pages');
  $groups =  og_all_groups_options();
  $form = og_notifications_add_form(NULL, $user, $groups);
  unset($form['account']);
  unset($form['subscription']['submit']);
  $form['subscription']['send_interval'][0]['#options'][-1] = 'User Default';
  $form['subscription']['send_interval'][0]['#default_value'] = -1;
  $form['subscription']['send_method'][0]['#options']['user_default'] =
    'User Default';
  $form['subscription']['send_method'][0]['#default_value'] = 'user_default';
  $form['overwrite_subscription'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace existing subscriptions with these settings.'),
    '#description' => t('If selected, any users who already have a ' .
      'subscriptiion will have this replaced with the specified settings'),
  );
  $form['verbose'] = notifications_utils_verbose_option();

  return $form;
}
/**
 * Implementation of "action_function"_validate hook
 * Validate the action parameters form.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_group_action_validate( $form,
                                                              $form_state) {
}
/**
 * Implementation of "action_function"_submit hook
 * Calls the common form function that uses notifications to process it.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function notifications_utils_subscribe_group_action_submit($form, $form_state) {
  $values = $form_state['values'];

  $results = array();

  $subscription = new stdClass;
  $subscription->type = 'grouptype';

  if ( $values['subscription']['send_method'][0] != 'user_default' ) {
    $subscription->send_method = $values['subscription']['send_method'][0];
  }
  if ( $values['subscription']['send_interval'][0] > -1 ) {
    $subscription->send_interval = $values['subscription']['send_interval'][0];
  }

  $subscription->fields = array(
    'group' => (string) $values['subscription']['group'][0],
  );

  if ($values['subscription']['node_type'][0] == 'all') {
    $types = array_filter(
      variable_get('og_notifications_content_types', array()));
    foreach ($types as $type) {
      $sub = clone $subscription;
      $sub->fields['type'] = $type;
      $results['subscriptions'][] = $sub;
    }
  }
  else {
    $subscription->fields['type'] = $values['subscription']['node_type'][0];
    $results['subscriptions'][] = $subscription;
  }
  $results['overwrite_subscription'] =
      ! empty($values['overwrite_subscription']);
  $results['verbose'] =
      ! empty($form_state['values']['verbose']);
// log_debug("results=" . print_r($results, TRUE));
  return $results;
}

/**
 * Implementation of Drupal "action_function".
 *
 * @param User $account
 * @param unknown_type $context
 */
function notifications_utils_subscribe_group_action(&$account, $context) {
  $overwrite = $context['overwrite_subscription'];
  $verbose = $context['verbose'];

  $subscriptions = $context['subscriptions'];
  foreach ( $subscriptions as $sub ) {
  	$subscription = clone $sub;
    if ( empty($account->og_groups[$subscription->fields['group']]) ) {
      if ( $verbose ) {
        drupal_set_message(t('User @user: Subscription not added because ' .
          'they are not a group member.',
          array('@user' => $account->name)), 'status');
      }
      return; // User is not member.. do nothing.
    }

    $update = FALSE;
    $params = array(
      'type' => $subscription->type,
      'uid' => $account->uid,
    );
    $conditions = array(
      'group' => $subscription->fields['group'],
      'type' => $subscription->fields['type'],
    );
    $subs = notifications_get_subscriptions($params, $conditions, TRUE, 'value');

    if ( ! empty( $subs ) ) {
      if ( ! $overwrite ) {
        if ( $verbose ) {
          drupal_set_message(t('User @user: Subscription not added because ' .
            'one already exists.', array('@user' => $account->name)), 'status');
        }
        continue;  // Already subscribed... do nothing.
      };
      $existing_sub = reset($subs);
      $subscription->sid = $existing_sub->sid;
      $update = TRUE;
    }

    $subscription->uid = $account->uid;

    //$sub = notifications_utils_convert_sub( $subscription );
    if ( notifications_user_allowed('subscription', $account, $subscription)) {
      $result = notifications_save_subscription($subscription);
      if ( $result === FALSE ) {
        $db_action = $update ? t("updated") : t("created");
        drupal_set_message(t('Subscription for @user was not @action.', array(
          '@user' => $account->name, '@action' => $db_action)), 'error');
      }
    }
    elseif ( $verbose ) {
      drupal_set_message(t('User @user: Subscription not added because ' .
        'they do not have correct permissions.',
        array('@user' => $account->name)), 'status');
    }
  }
}
