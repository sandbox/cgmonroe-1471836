<?php

/**
 * @file
 * Provides the administration pages for notifications_utils.
 */

/**
 * Implementation of hook_settings().
 */
function notifications_utils_admin_settings() {
  global $user;

  $modulepath = drupal_get_path('module', 'notifications_utils');

  $form['notification_utils_disable_notifications'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable all notifications.'),
    '#default_value' =>
      variable_get('notification_utils_disable_notifications', 0),
    '#description' => t('If enabled, no notifications will be sent to users.'),
  );

  $form['notifications_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notifications Fieldset Settings'),
    '#collapsible' => FALSE,
    '#description' =>
      t('The settings below will change the way that the "Notification" ' .
        'field set will be displayed on various forms.'),
  );

  $form['notifications_fieldset']['notifications_utils_send_new'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notifications for new nodes/comments.'),
    '#default_value' =>
      variable_get('notifications_utils_send_new', 1),
    '#description' => t('If not selected, the "Do Not Send Notifications ' .
      'for this Update" option on content/comment forms will ' .
      'default to being checked for new content.'),
  );

  $form['notifications_fieldset']['notifications_utils_send_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notifications for updated nodes/comments'),
    '#default_value' =>
      variable_get('notifications_utils_send_update', 1),
    '#description' => t('If not selected, the "Do Not Send Notifications ' .
      'for this Update" option on content/comment forms will ' .
      'default to being checked for updated content.'),
  );

  $form['notifications_fieldset']['notifications_utils_collapse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapse the notifications fieldset by default.'),
    '#default_value' =>
      variable_get('notifications_utils_collapse', 0),
    '#description' => t('If selected, the Notifications fieldset ' .
      'will be displayed collapsed rather than the default of expanded.'),
  );
  $form['notifications_fieldset']['notifications_utils_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the fieldset display weight'),
    '#default_value' =>
      variable_get('notifications_utils_weight', -10),
    '#description' => t('Define the default display weight to use with the ' .
      'Notifications fieldset. Low (including negative) numbers will make ' .
      'the fieldset be positioned nearer the top.  This may be overridden on ' .
      'node forms by the CCK Content Type fields weighting order.'),
    '#size' => 5,
    '#maxlength' => 10,
  );

  $form['users_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Messaging and Notifications Settings'),
    '#collapsible' => FALSE,
    '#description' =>
      t('The settings below will change the way that the "Messaging and ' .
        'Notification Settings" fieldset in the Edit User Account page ' .
        'will be displayed.'),
  );

  $form['users_fieldset']['notifications_utils_update_subscriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable update all subscriptions option.'),
    '#default_value' =>
      variable_get('notifications_utils_update_subscriptions', 0),
    '#description' => t('If enabled, user will have an option to update all ' .
      'their existing subscriptions when they change their default ' .
      'send interval or messaging method.'),
  );

  $form['users_fieldset']['notifications_default_update_subscriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable subscription updating by default.'),
    '#default_value' =>
      variable_get('notifications_default_update_subscriptions', 0),
    '#description' => t('If enabled, the update all subscription option will ' .
      'be checked by default.'),
  );

  $form['notifications_utils_subscription_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the improved messages for the Subscriptions block'),
    '#default_value' =>
      variable_get('notifications_utils_subscription_block', '0'),
    '#description' => t('If selected, the Notification UI Subscription '.
      'block will have an instructions section added and the option ' .
      'descriptions will be less technical.'),
  );

  $form['#validate'] = array('notifications_utils_admin_settings_validate');
  return system_settings_form($form);
}

/**
 * Form API callback to validate the notifications_utils settings form.
 */
function notifications_utils_admin_settings_validate($form, $form_state) {
}