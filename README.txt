Notifications_Utils:  README.txt
================================

Dependencies:
- Notifications module, http://drupal.org/project/notifications
- Views module, http://drupal.org/project/views
- Views Bulk Operations, http://drupal.org/project/views_bulk_operations

Supports:
- Notifications Content module (part of notifications project)
- Notifications Taxomony module (part of notifications project)
- OG Notifications module (part of Organic Groups project)
- CCK Manage Fields support (part of CCK's content module)

Overview
========

The notification_utils module is an "add-on" to the Notifications module.  It
adds various features and formating options that make managing sites using 
Notifications easier.

Views Bulk Operations Subscription Actions
==========================================

This module supplies Views Bulk Operations actions to subscribe and unsubscribe
multiple users to various notification events. The suppored events are:

- Subscribe/Unsubscribe to a content type (nodetype events)
- Subscribe/Unsubscribe to a single node's comment thread (thread events)
- Subscribe/Unsubscribe to an author's posts (author events)
- Subscribe/Unsubscribe to an author's posts of a specific type (typeauthor events)
- Subscribe/Unsubscribe to a group's content (grouptype events)
- Subscribe/Unsubscribe to a taxonomy term (taxonomy events)
- Modify a user's default send method and/or interval

Notes:

The supporting module and notificatons type must be enabled for the 
subscribe/unsubscribe actions to be available.

Also, the current user must have the 'administer notifications' permission. 

The actions will call notifications_user_allowed to determine the user's right 
to subscribe (e.g. group member, node access) and will not create subscriptions 
for these users.  It is suggest not to depend heavily on this, since there 
may still be conditions not checked.  I.e., don't be afraid to use it, just 
don't do broad stroke changes if you care about specific content security.

There is a "verbose" option on the subscribe actions that will cause Drupal
messages to be displayed the reasons a user was not subscribed (e.g., no
permissions, existing subscription, and the like.).  Note that this option
may produce a very long message list.

To use these v.b.o. actions, you will need to:

 - Create a view of type user with at least the user->name field.
 - Set the Display Style to Bulk Operations
 - Use the Bulk Operations settings to select the appropriate subscribe and
   unsubscribe actions to be used.
   
Extra Credit: Add some more fields and exposed filters to easily generate  
desired subsets (e.g. all users in a Role, etc).

CCK Manage Fields support 
=========================

This module implements CCK's content_extra_fields hook when the 
notifications_content module is enabled.  This lets the position of the
Notifications fieldset be set for each content type using the CCK Manage 
Fields display ( Admin -> Content -> Content Types -> <type> ).

Admin Page
==========

This module adds a new admin section to the Admin->Messaging & Notifications 
area labeled Notfications Utils.  This is where you can turn on or off various
features. 

Disabling All Notifications
===========================

There are times that you may want to suspend all notifications.  E.g., you are
doing a lot of updates and don't want to bother the subscribers.  Or there is 
a problem with the site you need to fix (i.e. blocking a persistent spammer).

Notification Fieldset Settings
==============================

The notification module adds a "Notification" field set to various forms that 
allows users to "Disable sending notifications".  However, this field often
appears in random locations on the form.  This module's admin page has options
to better control this field set.  Here are the options.

- Send notifications for new nodes/comments.

  If selected, "Do not send notifications" will be enabled on all create new 
  content or comment forms.
  
- Send notifications for updated nodes/comments.

  If selected, "Do not send notifications" will be enabled on all update 
  content or comment forms.
   
- Collapse the notifications fieldset by default.

  If selected, the notifications fieldset will be collapsed and not expanded.
  
- Set the fieldset display weight

  This sets the default weight to use when the fields set is added to a form.  
  Higher (heavier) numbers cause the field set to move lower, lower (lighter) 
  numbers cause the field set to be higher in the list.
  
 
Subscription Block Messages
===========================
 
- Use improved message for the Subscription Block

  The default "Subscription" messages are not very end user friendly.  The 
  terminology of "Post of type XXXX" and "This post" are not very clear to 
  users who no nothing of content types, etc.
  
  Enabling this option will reformat the block to:
  
  o Include a paragraph of instructions before the options.  E.g.:
  
    "Use the options below to control which notifications you will get from 
     this part of the system.  If an option below is checked...  
     
  o The labels for the options will be changed to have more verbiage.  E.g.:
  
    "This post" => "Get notified when a new comment is posted about this post"
    "Post of this type..." => "Get notified when new @types are created."
    "<type> in <group>" => "Get notified when new @types are created in @group group."

TODO: Theme the messages.    
    
  